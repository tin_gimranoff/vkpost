from django.shortcuts import render

from account.models import Account


def index(request):
    if Account.check_account(Account, request=request) is False:
        return Account.redirect_to_auth(Account)
    meta = {
        'title': u'Личный кабинет'
    }
    return render(request, 'index.html', {
        'meta': meta
    })
