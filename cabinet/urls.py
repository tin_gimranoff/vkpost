from django.urls import path

from cabinet import views

urlpatterns = [
    path('', views.index),
]