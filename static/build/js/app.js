$(document).ready(function () {

    $(".overlay").hide();
    //Создание и удаление профиля
    $("#newProfileModal .btn-send").bind('click', function () {
        var profie_name = $("#newProfileModal input[name=profile_name]").val();
        if (profie_name == '') {
            $("#newProfileModal .form-group").addClass('has-error');
            $("#newProfileModal .invalid-feedback").empty().append("Название должно быть заполнено.");
        } else {
            $("#newProfileModal .form-group").removeClass('has-error');
            $("#newProfileModal .invalid-feedback").empty();
        }
        $.ajax({
            url: '/post/add_profile/',
            type: 'post',
            data: {profile_name: profie_name},
            dataType: 'json',
            beforeSend: function () {
                $(".overlay").show();
            },
            complete: function () {
                $(".overlay").hide();
            },
            success: function (data) {
                if (data.status == -1) {
                    window.location.href = '/account/login/';
                    return;
                }
                if (data.status == 0) {
                    $("#newProfileModal .form-group").addClass('has-error');
                    $("#newProfileModal .invalid-feedback").empty().append(data.error);
                } else {
                    $("#newProfileModal .form-group").removeClass('has-error');
                    $("#newProfileModal .invalid-feedback").empty();
                    $('#newProfileModal').modal('hide');
                    $("#profile_select").empty().append(data.html);
                    $("#profile_message").val('');
                }
            },
        });
    });

    $('#newProfileModal').on('hidden.bs.modal', function (e) {
        $("#newProfileModal .form-group").removeClass('has-error')
        $("#newProfileModal input[name=profile_name]").val('');
        $("#newProfileModal .invalid-feedback").empty();
    });

    //Конец создание и удаление профиля

    //Создание новой базы
    $("#newBaseModal .btn-send").bind('click', function () {
        var base_name = $("#newBaseModal input[name=base_name]").val();
        if (base_name == '') {
            $("#newBaseModal .form-group").addClass('has-error');
            $("#newBaseModal .invalid-feedback").empty().append("Название должно быть заполнено.");
        } else {
            $("#newBaseModal .form-group").removeClass('has-error');
            $("#newBaseModal .invalid-feedback").empty();
        }
        $.ajax({
            url: '/post/add_base/',
            type: 'post',
            data: {base_name: base_name},
            dataType: 'json',
            beforeSend: function () {
                $(".overlay").show();
            },
            complete: function () {
                $(".overlay").hide();
            },
            success: function (data) {
                if (data.status == -1) {
                    window.location.href = '/account/login/';
                    return;
                }
                if (data.status == 0) {
                    $("#newBaseModal .form-group").addClass('has-error');
                    $("#newBaseModal .invalid-feedback").empty().append(data.error);
                } else {
                    $("#newBaseModal .form-group").removeClass('has-error');
                    $("#newBaseModal .invalid-feedback").empty();
                    $('#newBaseModal').modal('hide');
                    $("#base_select").empty().append(data.html);
                    $("#base_select").trigger('change');
                }
            }
        });
    });

    $('#newBaseModal').on('hidden.bs.modal', function (e) {
        $("#newBaseModal .form-group").removeClass('is-invalid')
        $("#newBaseModal input[name=base_name]").val('');
        $("#newBaseModal .invalid-feedback").empty();
    });

    //Конец создания новой базы

    //Добавление сообщества в Базу
    $("#addGroupModal .btn-send").bind('click', function () {
        var group_name = $("#addGroupModal input[name=group_name]").val();
        if (group_name == '') {
            $("#addGroupModal .form-group").addClass('has-error');
            $("#addGroupModal .invalid-feedback").empty().append("Адрес должен быть заполнен.");
            return;
        } else {
            $("#addGroupModal .form-group").removeClass('has-error');
            $("#addGroupModal .invalid-feedback").empty();
        }
        var base_id = $("#base_select").val();
        $.ajax({
            url: '/post/add_group/',
            type: 'post',
            data: {link: group_name, base_id: base_id},
            dataType: 'json',
            beforeSend: function () {
                $(".overlay").show();
            },
            complete: function () {
                $(".overlay").hide();
            },
            success: function (data) {
                if (data.status == -1) {
                    window.location.href = '/account/logout/';
                    return;
                }
                if (data.status == 0) {
                    $("#addGroupModal .form-group").addClass('has-error');
                    $("#addGroupModal .invalid-feedback").empty().append(data.error);
                } else {
                    base_select($("#base_select"));
                    $('#addGroupModal').modal('toggle');
                }
            }
        });
    });

    $('#addGroupModal').on('hidden.bs.modal', function (e) {
        $("#addGroupModal input[name=group_name]").val('');
        $("#addGroupModal .form-group").removeClass('has-error');
        $("#addGroupModal .invalid-feedback").empty();
    });
    //Конец добавления группы в базу

    $("#send_msg").bind('click', function () {
        start_posting(undefined, undefined, undefined, 1);
    });

    $("#base_select").bind('change', function () {
        base_select(this);
    });

    $("#profile_select").bind('change', function () {
        var profile = $(this).val();
        $.cookie('profile_select', profile, {path: '/'});
        $.ajax({
            url: '/post/get_message/',
            type: 'post',
            data: {profile: profile},
            dataType: 'json',
            beforeSend: function () {
                $(".overlay").show();
            },
            complete: function () {
                $(".overlay").hide();
            },
            success: function (data) {
                if (data.status == -1) {
                    window.location.href = '/account/login/';
                    return;
                }
                $(".file-list").empty();
                if (data.message == false) {
                    $("#profile_message").val('');
                    return;
                }
                $("#profile_message").val(data.message);
                if (data.photos.length > 0) {
                    $.each(data.photos, function (i, item) {
                        $(".file-list").append(
                            '<li type="photo" filename="' + item + '">Фото: <a href="/static/uploads/' + data.vk_account + '/' + item + '" target="_blank">' + item + '</a><a href="#" class="remove-file-link" onclick="remove_file(this)"><i class="fa fa-remove"></i></a></li>'
                        );
                    });
                }
                if (data.videos.length > 0) {
                    $.each(data.videos, function (i, item) {
                        $(".file-list").append(
                            '<li type="video" filename="' + item + '">Видео: <a href="/static/uploads/' + data.vk_account + '/' + item + '" target="_blank">' + item + '</a><a href="#" class="remove-file-link" onclick="remove_file(this)"><i class="fa fa-remove"></i></a></li>'
                        );
                    });
                }
                if (data.docs.length > 0) {
                    $.each(data.docs, function (i, item) {
                        $(".file-list").append(
                            '<li type="doc" filename="' + item + '">Документ: <a href="/static/uploads/' + data.vk_account + '/' + item + '" target="_blank">' + item + '</a><a href="#" class="remove-file-link" onclick="remove_file(this)"><i class="fa fa-remove"></i></a></li>'
                        );
                    });
                }
            }
        });
    });

    //Редактирование группы

    $('#editGroupModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var gid = button.data('gid') // Extract info from data-* attributes
        var base = button.data('base')
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-body #gid').val(gid)
        modal.find('.modal-body #base').val(base)
    });

    $("#update_group_btn").bind('click', function () {
        address = $("#gaddress").val();
        gid = $("#gid").val();
        base = $("#base").val();
        if (address == '') {
            $("#gaddress").parent().addClass('has-error');
            $("#editGroupModal .invalid-feedback").empty().append('Нужно ввести новый адрес').show();
            return;
        }
        $.ajax({
            url: '/groups/update/',
            type: 'post',
            data: {gid: gid, address: address, base: base},
            dataType: 'json',
            beforeSend: function () {
                $(".overlay").show();
            },
            complete: function () {
                $(".overlay").hide();
            },
            success: function (data) {
                if (data.status == -1) {
                    window.location.href = '/account/logout/';
                    return;
                }
                if (data.status == 0) {
                    $("#editGroupModal .form-group").addClass('has-error');
                    $("#editGroupModal .invalid-feedback").empty().append(data.error).show();
                } else {
                    $("#editGroupModal .form-group").removeClass('is-invalid');
                    $("#editGroupModal .invalid-feedback").empty().hide();
                    $('#editGroupModal').modal('hide');
                    base_select($("#base_select"));
                }
            }
        });
    });

    $('#editGroupModal').on('hidden.bs.modal', function (e) {
        $("#editGroupModal .form-group").removeClass('has-error');
        $("#editGroupModal #gaddress").val('');
        $("#addGroupModal .invalid-feedback").empty().hide();
    });
    //Конец обновления группы

    //Удаление профиля
    $("#delete_profile").bind('click', function () {
        if ($("#profile_select").val() == '') {
            show_info_message('Не выбран профиль');
            return;
        }
        $.ajax({
            url: '/post/delete_profile/' + $("#profile_select").val() + '/',
            type: 'post',
            dataType: 'json',
            beforeSend: function () {
                $(".overlay").show();
            },
            complete: function () {
                $(".overlay").hide();
            },
            success: function (data) {
                if (data.status == -1) {
                    window.location.href = '/account/login/';
                    return;
                }
                if (data.status == 1) {
                    location.reload();
                } else {
                    show_info_message(data.textError);
                }
            }
        });
    });
    //Конец удаление профилья

    //Удаление Базы
    $("#delete_base").bind('click', function () {
        if ($("#base_select").val() == '') {
            show_info_message('Не выбрана база');
            return;
        }
        $.ajax({
            url: '/post/delete_base/' + $("#base_select").val() + '/',
            type: 'post',
            dataType: 'json',
            beforeSend: function () {
                $(".overlay").show();
            },
            complete: function () {
                $(".overlay").hide();
            },
            success: function (data) {
                if (data.status == -1) {
                    window.location.href = '/account/login/';
                    return;
                }
                if (data.status == 1) {
                    location.reload();
                } else {
                    show_info_message(data.textError);
                }
            }
        });
    });

    //Конец удаления базы
    //Удаления группы
    $(".panel_group_list").on('click', '.group_delete_button', function () {
        var list = [];
        $.each($("input[name='table_records']:checked"), function (index, i) {
            list.push($(i).val());
        });

        if (list.length == 0) {
            show_info_message('Вы не выбрали ни одного сообщества.');
            return
        }
        base_select($("#base_select"), list);
    });
    //Конец удаления групп

    if ($.cookie('base_select') != undefined && $.cookie('base_select') != '' && $("#base_select option[value=" + $.cookie('base_select') + "]").length != 0) {
        $("#base_select").val($.cookie('base_select')).trigger('change');
    }

    if ($.cookie('profile_select') != undefined && $.cookie('profile_select') != '' && $("#profile_select option[value=" + $.cookie('profile_select') + "]").length != 0) {
        $("#profile_select").val($.cookie('profile_select')).trigger('change');
    }

    //Форма контакта
    $(function () {
        $('.contact-form').parsley().on('field:validated', function () {
            var ok = $('.parsley-error').length === 0;
        }).on('form:submit', function () {
            var name = $("#contactForm input[name='name']").val();
            var contact = $("#contactForm input[name='contact']").val();
            var subject = 'Форма из кабинета';
            var message = $("#contactForm textarea").val();
            var token = $("#contactForm input[name='csrfmiddlewaretoken']").val();
            console.log(name, contact);
            $.ajax({
                url: '/',
                type: 'post',
                dataType: 'text',
                data: {name: name, email: contact, subject: subject, message: message, csrfmiddlewaretoken: token},
                beforeSend: function () {
                    $(".overlay").show();
                },
                complete: function () {
                    $(".overlay").hide();
                },
                success: function (data) {
                    console.log(data);
                    if (data == 'OK') {
                        $('.bs-callout-info').css('display', 'block');
                        $('.contact-form').css('display', 'none');
                        $('#contactForm .btn-primary').css('display', 'none');
                    }
                }
            });
            return false; // Don't submit form for this demo
        });
    });
    $('#contactForm').on('hidden.bs.modal', function (e) {
        $('.bs-callout-info').css('display', 'none');
        $('.contact-form').css('display', 'block');
        $('#contactForm .btn-primary').css('inline-display', 'block');
        $("#contactForm input, #contactForm textarea").val('');
        $('.contact-form').parsley().reset();
    });
    //Конец формы
});

function base_select(El, deleted_objects = undefined) {
    console.log(deleted_objects);
    var base = $(El).val();
    $.cookie('base_select', base, {path: '/'});
    $.ajax({
        url: '/groups/list/' + base + '/',
        type: deleted_objects == undefined ? 'get' : 'post',
        data: {'gid': deleted_objects},
        dataType: 'json',
        beforeSend: function () {
            $(".overlay").show();
        },
        complete: function () {
            $(".overlay").hide();
        },
        success: function (data) {
            if (data.status == -1)
                window.location.href = '/account/logout/'
            else {
                $(".panel_group_list .alert").hide();
                $(".panel_group_list").empty().append(data.html)
            }
            // iCheck
            if ($("input.flat")[0]) {
                $('input.flat').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });
            }
            // /iCheck
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".panel_group_list").empty();
            if (jqXHR.status == 404) {
                $("<div class='alert alert-info'>База сообществ не выбрана. Выберите базу в окне слева.</div>").appendTo(".panel_group_list");
            } else {
                $("<div class='alert alert-danger'>" + errorThrown + "</div>").appendTo(".panel_group_list");
            }
        }
    });
}

function start_posting(last_succes_group_id, captcha_sid, captcha_key, start) {
    var profile = $("#profile_select").val();
    var base = $("#base_select").val();
    var message = $("#profile_message").val();
    if (profile == '' || base == '' || message == '') {
        $("#message_error").show().empty().append('Профиль, база и сообщение должны быть выбраны и заполнены.');
        return false;
    }
    $("#message_error").hide().empty();

    var fileList = {docs: [], photos: [], videos: []}
    $.each($("ul.file-list li"), function (i, item) {
        if ($(item).attr('type') == 'photo')
            fileList.photos.push($(item).attr('filename'));
        if ($(item).attr('type') == 'video')
            fileList.videos.push($(item).attr('filename'))
        if ($(item).attr('type') == 'doc')
            fileList.docs.push($(item).attr('filename'))
    });

    $.ajax({
        url: '/post/send_post/',
        type: 'post',
        data: {
            fileList: fileList,
            profile: profile,
            base: base,
            message: message,
            last_succes_group_id: last_succes_group_id,
            captcha_sid: captcha_sid,
            captcha_key: captcha_key,
            start: start,
        },
        dataType: 'json',
        beforeSend: function () {
            $(".overlay").show();
        },
        complete: function () {
            $(".overlay").hide();
        },
        success: function (response) {
            if (response.status == -1) {
                window.location.href = '/account/logout/';
                return;
            }
            show_info_message(response.html);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR, textStatus, errorThrown);
            show_info_message(jqXHR.responseText);
        }
    });
}

function continue_posting() {
    var captcha_code = $("#captcha_code").val();
    var captcha_sid = $("#captcha_sid").val();
    var g_id = $("#group_id").val();

    start_posting(g_id, captcha_sid, captcha_code, 0);
}

function show_info_message(content) {
    $("#infoModal .modal-body").empty().append(content);
    $("#infoModal").modal('show');
}