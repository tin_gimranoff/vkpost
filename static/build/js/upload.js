window.onload = function (ev) {
    $('#message-image').fileupload({"maxFileSize": 2000000, "dataType": "json", "url": "/post/upload_image/"});
    $('#message-image').on('fileuploaddone', function (e, data) {
        if (typeof(data.result.status) != undefined && data.result.status == -1) {
            window.location.href = '/account/login/'
            return;
        }
        if (typeof(data.result.status) != undefined && data.result.status == 0) {
            show_info_message(data.result.textError);
        } else {
            $(".file-list").append(
                '<li type="photo" filename="' + data.result.name + '">Фото: <a href="' + data.result.url + '" target="_blank">' + data.result.name + '</a><a href="#" class="remove-file-link" onclick="remove_file(this)"><i class="fas fa-times"></i></a></li>'
            );
        }
    });

    $('#message-video').fileupload({"maxFileSize": 2000000, "dataType": "json", "url": "/post/upload_video/"});
    $('#message-video').on('fileuploaddone', function (e, data) {
        if (typeof(data.result.status) != undefined && data.result.status == -1) {
            window.location.href = '/account/login/'
            return;
        }
        if (typeof(data.result.status) != undefined && data.result.status == 0) {
            show_info_message(data.result.textError);
        } else {
            $(".file-list").append(
                '<li type="video" filename="' + data.result.name + '">Видео: <a href="' + data.result.url + '" target="_blank">' + data.result.name + '</a><a href="#" class="remove-file-link" onclick="remove_file(this)"><i class="fas fa-times"></i></a></li>'
            );
        }
    });

    $('#message-doc').fileupload({"maxFileSize": 2000000, "dataType": "json", "url": "/post/upload_doc/"});
    $('#message-doc').on('fileuploaddone', function (e, data) {
        if (typeof(data.result.status) != undefined && data.result.status == -1) {
            window.location.href = '/account/login/'
            return;
        }
        if (typeof(data.result.status) != undefined && data.result.status == 0) {
            show_info_message(data.result.textError);
        } else {
            $(".file-list").append(
                '<li type="doc" filename="' + data.result.name + '">Документ: <a href="' + data.result.url + '" target="_blank">' + data.result.name + '</a><a href="#" class="remove-file-link" onclick="remove_file(this)"><i class="fas fa-times"></i></a></li>'
            );
        }
    });

    $('#message-image, #message-video, #message-doc').on('fileuploadfail', function (e, data) {
        show_info_message("В процессе загрузки файла произошла ошибка");
    });
}

function remove_file(El) {
    event.preventDefault();
    $(El).parent().remove();
}