from django import forms

class LoginForm(forms.Form):
    login = forms.CharField(
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': u'Логин, телефон, email',
        }),
        required=True
    )

    password = forms.CharField(
        widget=forms.PasswordInput(attrs={
            'class': 'form-control',
            'placeholder': u'Пароль',
        }),
        required=True
    )