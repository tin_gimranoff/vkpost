from django.db import models
from django.shortcuts import redirect

from vkpost.private_config import SESSION_DOMAIN


class Account(models.Model):

    def __str__(self):
        return self.login

    class Meta:
        db_table = 'account'
        verbose_name = 'Учетная запись'
        verbose_name_plural = 'Учетные записи'

    login = models.CharField(max_length=255, verbose_name=u'Логин', null=False, blank=False, unique=False)
    password = models.CharField(max_length=255, verbose_name=u'Пароль', null=False, blank=False)
    vk_id = models.CharField(max_length=255, verbose_name=u'ID VK', null=False, blank=False, default='')
    first_name = models.CharField(max_length=255, verbose_name=u'Имя', null=True, blank=True)
    last_name = models.CharField(max_length=255, verbose_name=u'Фамилия', null=True, blank=True)
    avatar = models.CharField(max_length=255, verbose_name=u'Фамилия', null=True, blank=True)
    token = models.CharField(max_length=255, verbose_name=u'Token', null=False, blank=False, default='')

    def check_account(self, request):
        if request.COOKIES.get('token') is None:
            return False

        try:
            self.objects.get(token=request.COOKIES.get('token'))
            return True
        except self.DoesNotExist:
            return False

        if account is None:
            return False

        return True

    def redirect_to_auth(self):
        response = redirect('/account/login/')
        response.delete_cookie('token', domain=SESSION_DOMAIN)
        return response


class AccountLog(models.Model):

    class Meta:
        db_table = 'account_log'

    account = models.ForeignKey(Account, on_delete=None, null=False, blank=False)
    group_name = models.CharField(max_length=255, null=False, blank=False)
    comment = models.CharField(max_length=255, null=False, blank=False)
