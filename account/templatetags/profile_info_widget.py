from django import template

from account.models import Account

register = template.Library()


@register.inclusion_tag('_sidebar_widget.html', takes_context=True)
def sidebar_widget(context):
    request = context['request']
    account = Account.objects.filter(token=request.COOKIES.get('token')).first()
    return {
        'avatar': account.avatar,
        'first_name': account.first_name,
        'last_name': account.last_name,
    }

@register.inclusion_tag('_top_nav_widget.html', takes_context=True)
def top_nav_widget(context):
    request = context['request']
    account = Account.objects.filter(token=request.COOKIES.get('token')).first()
    return {
        'avatar': account.avatar,
        'first_name': account.first_name,
        'last_name': account.last_name,
    }
