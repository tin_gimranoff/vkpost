import os
import hashlib
import time

import vk_api

from django.contrib import messages
from django.shortcuts import render, redirect

from account.forms import LoginForm
from account.models import Account
from vkpost.private_config import SESSION_DOMAIN
from vkpost.settings import BASE_DIR


def login(request):
    if request.COOKIES.get('token') is not None and request.COOKIES.get('token') is not None:
        response = redirect('/cabinet/')
        return response
    meta = {
        'title': u'Ввод данных пользователя в ВК'
    }
    captcha_img = None
    captcha_sid = None
    if request.method == 'POST':
        form = LoginForm(request.POST or None)
        captcha_code = request.POST.get('captcha', None)
        captcha_sid = request.POST.get('captcha_sid', None)
        if form.is_valid():
            form_data = form.clean()
            vk_session = vk_api.VkApi(form_data['login'], form_data['password'], config_filename=os.path.join(BASE_DIR, 'vk_config.json'))
            try:
                try:
                    if captcha_sid is not None and captcha_code is not None:
                        vk_session._vk_login(captcha_sid=captcha_sid, captcha_key=captcha_code)
                    else:
                        vk_session._vk_login()
                    vk_session.auth()
                    vk = vk_session.get_api()
                    profile = vk.users.get(fields='photo_50,first_name,last_name,id')
                    vk_id = profile[0]['id']
                    first_name = profile[0]['first_name']
                    last_name = profile[0]['last_name']
                    avatar = profile[0]['photo_50']
                    account = Account.objects.filter(
                        vk_id=vk_id,
                    ).first()
                    if account is None:
                        account = Account(
                            login=form_data['login'],
                            password=form_data['password'],
                            vk_id=vk_id,
                            first_name=first_name,
                            last_name=last_name,
                            avatar=avatar,
                            token=hashlib.md5(str(time.time()).encode()).hexdigest()
                        )
                        account.save()
                    else:
                        account.login = form_data['login']
                        account.password = form_data['password']
                        account.first_name = first_name
                        account.last_name = last_name
                        account.avatar = avatar
                        account.token = hashlib.md5(str(time.time()).encode()).hexdigest()
                        account.save()
                    response = redirect('/cabinet/')
                    response.set_cookie('token', account.token, domain=SESSION_DOMAIN)
                    return response
                except vk_api.exceptions.Captcha as captcha:
                    captcha_img = captcha.get_url()
                    captcha_sid = captcha.sid
            except vk_api.AuthError:
                messages.add_message(request, messages.WARNING, u'Не верно введен логин или пароль.')
        else:
            messages.add_message(request, messages.WARNING, u'В процессе произошла ошибка.')
    else:
        form = LoginForm
    response = render(request, 'login.html', {
        'form': form,
        'meta': meta,
        'captcha_img': captcha_img,
        'captcha_sid': captcha_sid,
    })
    return response


def logout(request):
    response = redirect('/account/login/')
    response.delete_cookie('token', domain=SESSION_DOMAIN)
    return response
