from django.urls import path

from files_from_dialog import views

urlpatterns = [
    path('', views.main),
]