from django.shortcuts import render


def main(request):
    meta = {
        'title': u'Вложение из бесед VK.COM'
    }
    return render(request, 'files_from_dialog.html', {
        'meta': meta
    })