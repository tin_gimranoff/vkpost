from django.urls import path

from post import views

urlpatterns = [
    path('', views.main),
    path('add_profile/', views.add_profile),
    path('add_base/', views.add_base),
    path('add_group/', views.add_group),
    path('send_post/', views.send_post),
    path('get_message/', views.get_message),
    path('upload_image/', views.upload_image),
    path('upload_video/', views.upload_video),
    path('upload_doc/', views.upload_doc),
    path('delete_profile/<int:id>/', views.delete_profile),
    path('delete_base/<int:id>/', views.delete_base),
]