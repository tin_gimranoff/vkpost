# Generated by Django 2.1.2 on 2018-10-15 19:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('post', '0005_profile_message'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='docs',
            field=models.TextField(blank=True, null=True, verbose_name='Документы'),
        ),
        migrations.AddField(
            model_name='profile',
            name='photos',
            field=models.TextField(blank=True, null=True, verbose_name='Фото'),
        ),
        migrations.AddField(
            model_name='profile',
            name='videos',
            field=models.TextField(blank=True, null=True, verbose_name='Видео'),
        ),
    ]
