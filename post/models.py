from django.db import models

from account.models import Account


class Profile(models.Model):

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'profiles'
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'

    name = models.CharField(max_length=255, null=False, blank=False, verbose_name=u'Название профиля')
    account = models.ForeignKey(Account, on_delete=False, blank=False, null=False, verbose_name=u'Аккаунт в социальной сети')
    message = models.TextField(null=True, blank=True, verbose_name=u'Сообщение')
    photos = models.TextField(null=True, blank=True, verbose_name=u'Фото')
    videos = models.TextField(null=True, blank=True, verbose_name=u'Видео')
    docs = models.TextField(null=True, blank=True, verbose_name=u'Документы')


class Base(models.Model):

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'bases'
        verbose_name = 'База'
        verbose_name_plural = 'Базы'

    name = models.CharField(max_length=255, null=False, blank=False, verbose_name=u'Название профиля')
    account = models.ForeignKey(Account, on_delete=False, blank=False, null=False, verbose_name=u'Аккаунт в социальной сети')

class Group(models.Model):

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'groups'
        verbose_name = 'Сообщество'
        verbose_name_plural = 'Сообщества'

    link = models.CharField(max_length=255, null=False, blank=False, verbose_name=u'Адрес сообщества')
    group_id = models.CharField(max_length=255, null=False, blank=False, verbose_name=u'ID группы')
    name = models.CharField(max_length=255, null=False, blank=False, default='', verbose_name=u'Название')
    base = models.ForeignKey(Base, on_delete=models.CASCADE, blank=False, verbose_name=u'База')

