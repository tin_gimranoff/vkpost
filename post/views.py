import ast
import os
import re
import vk_api
from django.core.files.storage import FileSystemStorage

from django.http import JsonResponse
from django.shortcuts import render
from time import time
from django.template.loader import get_template
from django.utils._os import safe_join
from django.views.decorators.csrf import csrf_exempt

from account.models import Account, AccountLog
from post.models import Profile, Base, Group
from vkpost.settings import BASE_DIR


def main(request):
    if Account.check_account(Account, request=request) is False:
        return Account.redirect_to_auth(Account)

    meta = {
        'title': u'Отправка постов в группы'
    }

    account = Account.objects.filter(token=request.COOKIES.get('token')).first()
    profiles = Profile.objects.filter(account=account).order_by('name').all()
    bases = Base.objects.filter(account=account).order_by('name').all()

    response = render(request, 'posting.html', {
        'meta': meta,
        'profiles': profiles,
        'bases': bases,
    })
    return response


@csrf_exempt
def add_profile(request):
    if Account.check_account(Account, request) is False:
        return JsonResponse({
            'status': -1,
        })
    if not request.is_ajax():
        return JsonResponse({
            'status': 0,
            'error': u'Это не AJAX запрос.',
        })

    profile_name = request.POST.get('profile_name', None)
    if profile_name is None or profile_name == '':
        return JsonResponse({
            'status': 0,
            'error': u'Название профиля не должно быть пустым.'
        })
    account = Account.objects.filter(token=request.COOKIES.get('token')).first()
    profile = Profile.objects.filter(account=account, name=profile_name).first()

    if profile is not None:
        return JsonResponse({
            'status': 0,
            'error': u'Такой профиль у вас уже есть. Введите новое название.'
        })

    profile = Profile(name=profile_name, account=account)
    try:
        profile.save()
        profiles = Profile.objects.filter(account=account).order_by('name').all()
        t = get_template('_values_drop_down.html')
        html_to_dropdown = t.render({
            'object_id': profile.id,
            'profiles': profiles,
        })
        return JsonResponse({
            'status': 1,
            'error': '',
            'object_id': profile.id,
            'html': html_to_dropdown,
        })
    except:
        return JsonResponse({
            'status': 0,
            'error': u'В процессе сохранения произошла ошибка',
        })


@csrf_exempt
def add_base(request):
    if Account.check_account(Account, request) is False:
        return JsonResponse({
            'status': -1,
        })
    if not request.is_ajax():
        return JsonResponse({
            'status': 0,
            'error': u'Это не AJAX запрос.',
        })
    base_name = request.POST.get('base_name', None)
    if base_name is None or base_name == '':
        return JsonResponse({
            'status': 0,
            'error': u'Название базы не должно быть пустым.'
        })
    account = Account.objects.filter(token=request.COOKIES.get('token')).first()
    base = Base.objects.filter(account=account, name=base_name).first()

    if base is not None:
        return JsonResponse({
            'status': 0,
            'error': u'Такая база у вас уже есть. Введите новое название.'
        })

    base = Base(name=base_name, account=account)
    try:
        base.save()
        bases = Base.objects.filter(account=account).order_by('name').all()
        t = get_template('_values_drop_down.html')
        html_to_dropdown = t.render({
            'object_id': base.id,
            'profiles': bases,
        })
        return JsonResponse({
            'status': 1,
            'error': '',
            'object_id': base.id,
            'html': html_to_dropdown,
        })
    except:
        return JsonResponse({
            'status': 0,
            'error': u'В процессе сохранения произошла ошибка',
        })


@csrf_exempt
def add_group(request):
    if Account.check_account(Account, request) is False:
        return JsonResponse({
            'status': -1,
        })
    if not request.is_ajax():
        return JsonResponse({
            'status': 0,
            'error': u'Это не AJAX запрос.',
        })
    base_id = request.POST.get('base_id', None)
    link = request.POST.get('link', None)

    if base_id is None or link is None or base_id == '' or link == '':
        return JsonResponse({
            'status': 0,
            'error': u'Не заполнено название группы или не выбрана база. Исправьте ошибки.',
        })

    parse_url = link.split('/')
    group_id = parse_url[len(parse_url) - 1]
    if re.search('^event[0-9]+$', group_id, flags=re.IGNORECASE):
        group_id = group_id.replace('event', '')
    if re.search('^club[0-9]+$', group_id, flags=re.IGNORECASE):
        group_id = group_id.replace('club', '')

    account = Account.objects.filter(token=request.COOKIES.get('token')).first()
    vk_session = vk_api.VkApi(account.login, account.password, config_filename=os.path.join(BASE_DIR, 'vk_config.json'))
    try:
        vk_session.auth()
    except vk_api.AuthError:
        return JsonResponse({
            'status': 0,
            'error': u'Ошибка при инициализации клиента ВК.',
        })
    vk = vk_session.get_api()
    try:
        group_info = vk.groups.getById(group_id=group_id, fields='can_post,name,is_member,is_closed,deactivated')
        if 'deactivated' in group_info[0]:
            return JsonResponse({
                'status': 0,
                'error': u'Сообщество по данному адресу заблокировано',
            })
        else:
            if group_info[0]['can_post'] == 0:
                error_message = 'Нельзя отсылать сообщения в данную группу.'
                if group_info[0]['is_closed'] == 1:
                    error_message += ' Сообщество закрытое.'
                if group_info[0]['is_closed'] == 2:
                    error_message += ' Сообщество частное.'
                if group_info[0]['is_member'] == 0:
                    error_message += ' Возможно для решения проблемы требуется вступить.'
                return JsonResponse({
                    'status': 0,
                    'error': error_message,
                })
            else:
                group = Group.objects.filter(group_id=group_info[0]['id'], base_id=base_id).first()
                if group is not None:
                    return JsonResponse({
                        'status': 0,
                        'error': u'Данное сообщество уже есть в данной базе. Введите адрес другого сообщества или поменяйте базу.',
                    })
                group = Group(name=group_info[0]['name'], group_id=group_info[0]['id'], link=link, base_id=base_id)
                group.save()
                return JsonResponse({
                    'status': 1,
                    'error': 'Сообщество успешно добавлено!',
                })
    except:
        return JsonResponse({
            'status': 0,
            'error': u'Введен не правильынй адрес.',
        })


@csrf_exempt
def send_post(request):
    if Account.check_account(Account, request) is False:
        return JsonResponse({
            'status': -1,
        })
    account = Account.objects.filter(token=request.COOKIES.get('token')).first()
    profile = request.POST.get('profile')
    base = request.POST.get('base')
    message = request.POST.get('message')

    captcha_key = request.POST.get('captcha_key', None)
    captcha_sid = request.POST.get('captcha_sid', None)
    last_succes_group_id = request.POST.get('last_succes_group_id', None)

    start = request.POST.get('start')

    photosList = request.POST.getlist('fileList[photos][]')
    videosList = request.POST.getlist('fileList[videos][]')
    docsList = request.POST.getlist('fileList[docs][]')

    if start == '1':
        AccountLog.objects.filter(account=account).delete()

    if len(videosList) + len(videosList) + len(docsList) > 10:
        t = get_template('_message.html')
        html = t.render({'message': 'Количество вложений не может быть больше 10'})
        return JsonResponse({
            'status': 0,
            'html': html,
        })

    profile = Profile.objects.filter(id=profile).first()
    profile.message = message
    profile.photos = photosList
    profile.videos = videosList
    profile.docs = docsList
    profile.save()

    base = Base.objects.filter(id=base).first()
    if last_succes_group_id is None:
        groups = Group.objects.filter(base=base).order_by('id').all()
    else:
        groups = Group.objects.filter(base=base, id__gte=last_succes_group_id).order_by('id').all()

    vk_session = vk_api.VkApi(account.login, account.password, config_filename=os.path.join(BASE_DIR, 'vk_config.json'))
    try:
        vk_session.auth()
        upload = vk_api.VkUpload(vk_session)
    except vk_api.AuthError:
        t = get_template('_message.html')
        html = t.render({'message': 'Ошибка при авторизации в VK. Выйдите из системы и водите еще раз.'})
        return JsonResponse({
            'status': 0,
            'html': html,
        })
    vk = vk_session.get_api()

    attachments = []
    if len(photosList) > 0:
        for photo in photosList:
            p = upload.photo_wall(  # Подставьте свои данные
                BASE_DIR + '/static/uploads/%s/%s' % (account.id, photo),
            )
            attachments.append('photo%s_%s' % (p[0]['owner_id'], p[0]['id']))
    if len(videosList) > 0:
        for video in videosList:
            v = upload.video(  # Подставьте свои данные
                BASE_DIR + '/static/uploads/%s/%s' % (account.id, video),
            )
            attachments.append('video%s_%s' % (v['owner_id'], v['video_id']))
    if len(docsList) > 0:
        for doc in docsList:
            d = upload.document(  # Подставьте свои данные
                BASE_DIR + '/static/uploads/%s/%s' % (account.id, doc),
                title=doc,
            )
            attachments.append('doc%s_%s' % (d[0]['owner_id'], d[0]['id']))
    for group in groups:
        try:
            if captcha_sid is None and captcha_key is None:
                vk.wall.post(owner_id='-%s' % group.group_id, message=message, attachments=','.join(attachments))
            else:
                vk.wall.post(owner_id='-%s' % group.group_id, message=message, captcha_sid=captcha_sid,
                             captcha_key=captcha_key, attachments=','.join(attachments))
            log = AccountLog(account=account, group_name=group.name, comment=u'Успешно')
            log.save()
        except vk_api.exceptions.Captcha as captcha:
            log = AccountLog(account=account, group_name=group.name, comment=u'Запрошена каптча')
            log.save()
            captcha_sid = captcha.sid
            captcha_img = captcha.get_url()

            t = get_template('_captcha.html')
            html = t.render({'captcha_sid': captcha_sid, 'captcha_img': captcha_img, 'g_id': group.id})
            return JsonResponse({
                'status': 0,
                'html': html,
            })
        except vk_api.ApiError as api_error:
            return JsonResponse({
                'status': 0,
                'html': '<div class="alert alert-danger"><strong>Ошибка:</strong> %s</div>' % api_error.error['error_msg'],
            })

    logs = AccountLog.objects.filter(account=account).order_by('id').all()
    t = get_template('_log.html')
    html = t.render({'logs': logs})
    return JsonResponse({
        'status': 0,
        'html': html,
    })


@csrf_exempt
def get_message(request):
    if Account.check_account(Account, request) is False:
        return JsonResponse({
            'status': -1,
        })
    account = Account.objects.filter(token=request.COOKIES.get('token')).first()
    profile = request.POST.get('profile', None)
    if profile is None or profile == '':
        return JsonResponse({
            'message': False,
        })

    profile = Profile.objects.filter(id=profile).first()
    return JsonResponse({
        'message': profile.message,
        'photos': ast.literal_eval(profile.photos) if profile.photos is not None else [],
        'videos': ast.literal_eval(profile.videos) if profile.videos is not None else [],
        'docs': ast.literal_eval(profile.docs) if profile.docs is not None else [],
        'vk_account': account.id,
    })


@csrf_exempt
def upload_image(request):
    if Account.check_account(Account, request) is False:
        return JsonResponse({
            'status': -1,
        })
    account = Account.objects.filter(token=request.COOKIES.get('token')).first()
    if request.is_ajax() and request.FILES['message_image']:
        image_file = request.FILES['message_image']
        source_filename, file_extension = os.path.splitext(image_file.name)
        if file_extension not in ['.jpg', '.png', '.jpeg', '.gif']:
            return JsonResponse({
                'status': 0,
                'textError': u'Вы можете загружать только файлы изображений с разрешениями png, jpg, jpeg.'
            })
        fs = FileSystemStorage()
        directory = '/static/uploads/%s/' % (account.id)
        if not os.path.exists(BASE_DIR + directory):
            os.makedirs(BASE_DIR + directory)
        uid = hex(int(time() * 10000000))[2:]
        target_file_name = uid + file_extension
        target_uploaded_file_name = fs.save(BASE_DIR + directory + target_file_name, image_file)
        target_uploaded_file_path = fs.url(target_uploaded_file_name)
        if target_uploaded_file_path:
            return JsonResponse({
                'name': target_file_name,
                'url': '/static/uploads/%s/%s' % (account.id, target_file_name),
            })


@csrf_exempt
def upload_video(request):
    if Account.check_account(Account, request) is False:
        return JsonResponse({
            'status': -1,
        })
    account = Account.objects.filter(token=request.COOKIES.get('token')).first()
    if request.is_ajax() and request.FILES['message_video']:
        image_file = request.FILES['message_video']
        source_filename, file_extension = os.path.splitext(image_file.name)
        if file_extension not in ['.mp4', '.mov', '.avi', '.mpeg', '.wmv']:
            return JsonResponse({
                'status': 0,
                'textError': u'Вы можете загружать только файлы изображений с разрешениями mp4, avi, mov, mpeg, wmv.'
            })
        fs = FileSystemStorage()
        directory = '/static/uploads/%s/' % (account.id)
        if not os.path.exists(BASE_DIR + directory):
            os.makedirs(BASE_DIR + directory)
        uid = hex(int(time() * 10000000))[2:]
        target_file_name = uid + file_extension
        target_uploaded_file_name = fs.save(BASE_DIR + directory + target_file_name, image_file)
        target_uploaded_file_path = fs.url(target_uploaded_file_name)
        if target_uploaded_file_path:
            return JsonResponse({
                'name': target_file_name,
                'url': '/static/uploads/%s/%s' % (account.id, target_file_name),
            })


@csrf_exempt
def upload_doc(request):
    if Account.check_account(Account, request) is False:
        return JsonResponse({
            'status': -1,
        })
    account = Account.objects.filter(token=request.COOKIES.get('token')).first()
    if request.is_ajax() and request.FILES['message_doc']:
        image_file = request.FILES['message_doc']
        source_filename, file_extension = os.path.splitext(image_file.name)
        if file_extension not in ['.doc', '.docx', '.xls', '.xlsx', '.txt']:
            return JsonResponse({
                'status': 0,
                'textError': u'Вы можете загружать только файлы изображений с разрешениями doc, docx, xls, xlsx, txt'
            })
        fs = FileSystemStorage()
        directory = '/static/uploads/%s/' % (account.id)
        if not os.path.exists(BASE_DIR + directory):
            os.makedirs(BASE_DIR + directory)
        uid = hex(int(time() * 10000000))[2:]
        target_file_name = uid + file_extension
        target_uploaded_file_name = fs.save(BASE_DIR + directory + target_file_name, image_file)
        target_uploaded_file_path = fs.url(target_uploaded_file_name)
        if target_uploaded_file_path:
            return JsonResponse({
                'name': target_file_name,
                'url': '/static/uploads/%s/%s' % (account.id, target_file_name),
            })


@csrf_exempt
def delete_profile(request, id):
    safe_join
    if not request.is_ajax():
        return JsonResponse({
            'status': 0,
            'textError': u'Это не аякс запрос.',
        })
    if Account.check_account(Account, request) is False:
        return JsonResponse({
            'status': -1,
        })

    if Profile.objects.filter(id=id).first().delete():
        return JsonResponse({
            'status': 1,
            'textError': u'',
        })
    else:
        return JsonResponse({
            'status': 0,
            'textError': u'В проессе удаления произошла ошибка',
        })


@csrf_exempt
def delete_base(request, id):
    if not request.is_ajax():
        return JsonResponse({
            'status': 0,
            'textError': u'Это не аякс запрос.',
        })
    if Account.check_account(Account, request) is False:
        return JsonResponse({
            'status': -1,
        })

    if Base.objects.filter(id=id).first().delete():
        return JsonResponse({
            'status': 1,
            'textError': u'',
        })
    else:
        return JsonResponse({
            'status': 0,
            'textError': u'В проессе удаления произошла ошибка',
        })
