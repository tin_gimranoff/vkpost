from django.core.mail import send_mail
from django.http import HttpResponse
from django.shortcuts import render
from vkpost.settings import ADMIN_EMAIL, FROM_EMAIL, BASE_DIR
from mainpage.forms import ContactForm


def mainpage(request):
    meta = {
        'title': u'Полезные сервисы для вас',
        'keywords': u'vk bot,spam-bot,vk,вконтакте,сообщения,рассылка,скачать музыку в вк',
        'description': u'Сервисы расширющие функционал VK и других сайтов',
    }
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']
            message = "Новый запрос от %s\nКонтакт для связи: %s\nТема: %s\nВопрос: %s" % \
                      (
                          name,
                          email,
                          subject,
                          message
                      )
            send_mail('Новый сообщение с сайта Projectin.ru', message, FROM_EMAIL, [ADMIN_EMAIL])
            return HttpResponse("OK")
        else:
            return HttpResponse("Не правильно введен код с каптчи")

    else:
        form = ContactForm

    return render(request, 'landing.html', {
        'meta': meta,
        'form': form,
    })
