from captcha.fields import CaptchaField
from django import forms


class ContactForm(forms.Form):

    name = forms.CharField(min_length=4, required=True, widget=forms.TextInput(attrs={
        'class': 'form-control br-radius-zero',
        'id': 'name',
        'placeholder': u'Ваше имя',
        'data-rule': 'minlen:4',
        'data-msg': u'Введите минимум 4 символа',
    }))
    email = forms.EmailField(required=True, widget=forms.EmailInput(attrs={
        'class': 'form-control br-radius-zero',
        'id': 'email',
        'placeholder': u'Ваш Email',
        'data-rule': 'email',
        'data-msg': 'Введите корректный Email'
    }))
    subject = forms.CharField(required=True, min_length=4, widget=forms.TextInput(attrs={
        'class': 'form-control br-radius-zero',
        'id': 'subject',
        'placeholder': u'Тема',
        'data-rule': 'minlen:4',
        'data-msg': u'Минимум 4 символа для темы',
    }))
    message = forms.CharField(required=True, widget=forms.Textarea(attrs={
        'class': 'form-control br-radius-zero',
        'rows': 5,
        'data-rule': 'required',
        'data-msg': u'Напишите что-нибудь нам',
        'placeholder': u'Сообщение'
    }))
    captcha = CaptchaField()