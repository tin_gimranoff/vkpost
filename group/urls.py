from django.urls import path

from group import views

urlpatterns = [
    path('list/<int:id>/', views.list),
    path('update/', views.update),
]