import os
import re
import vk_api

from django.http import JsonResponse
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt

from post.models import Base, Group
from account.models import Account
from vkpost.settings import BASE_DIR


@csrf_exempt
def list(request, id):
    if Account.check_account(Account, request=request) is False:
        return JsonResponse({
            'status': -1,
        })

    if request.method == 'POST':
        gid = request.POST.getlist('gid[]')
        print(gid)
        Group.objects.filter(id__in=gid).delete()

    base = Base.objects.filter(id=id).first()

    groups = Group.objects.filter(base=base).all()

    response = render_to_string('list.html', {
        'groups': groups,
        'base': id,
    })
    return JsonResponse({
        'status': 1,
        'html': response
    })


@csrf_exempt
def update(request):
    if Account.check_account(Account, request) is False:
        return JsonResponse({
            'status': -1,
        })
    if not request.is_ajax():
        return JsonResponse({
            'status': 0,
            'textMessage': u'Это не AJAX запрос.',
        })

    gid = request.POST.get('gid', None)
    link = request.POST.get('address', None)
    base = request.POST.get('base', None)

    parse_url = link.split('/')
    group_id = parse_url[len(parse_url) - 1]
    if re.search('^event[0-9]+$', group_id, flags=re.IGNORECASE):
        group_id = group_id.replace('event', '')
    if re.search('^club[0-9]+$', group_id, flags=re.IGNORECASE):
        group_id = group_id.replace('club', '')

    account = Account.objects.filter(token=request.COOKIES.get('token')).first()
    vk_session = vk_api.VkApi(account.login, account.password, config_filename=os.path.join(BASE_DIR, 'vk_config.json'))
    try:
        vk_session.auth()
    except vk_api.AuthError:
        return JsonResponse({
            'status': 0,
            'error': u'Ошибка при инициализации клиента ВК.',
        })
    vk = vk_session.get_api()
    try:
        group_info = vk.groups.getById(group_id=group_id, fields='can_post,name,is_member,is_closed,deactivated')
        if 'deactivated' in group_info[0]:
            return JsonResponse({
                'status': 0,
                'error': u'Сообщество по данному адресу заблокировано',
            })
        else:
            if group_info[0]['can_post'] == 0:
                error_message = 'Нельзя отсылать сообщения в данную группу.'
                if group_info[0]['is_closed'] == 1:
                    error_message += ' Сообщество закрытое.'
                if group_info[0]['is_closed'] == 2:
                    error_message += ' Сообщество частное.'
                if group_info[0]['is_member'] == 0:
                    error_message += ' Возможно для решения проблемы требуется вступить.'
                return JsonResponse({
                    'status': 0,
                    'error': error_message,
                })
            else:
                g = Group.objects.filter(group_id=group_info[0]['id'], base_id=base).first()
                if g is not None:
                    return JsonResponse({
                        'status': 0,
                        'error': u'Данное сообщество уже есть в данной базе. Введите адрес другого сообщества или поменяйте базу.',
                    })
                group = Group.objects.filter(id=gid).first()
                group.name = group_info[0]['name']
                group.group_id = group_info[0]['id']
                group.link = link
                group.save()
                return JsonResponse({
                    'status': 1,
                    'error': 'Сообщество успешно обновлено!',
                })
    except:
        return JsonResponse({
            'status': 0,
            'error': u'Введен не правильынй адрес.',
        })
