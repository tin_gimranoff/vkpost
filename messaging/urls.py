from django.urls import path

from messaging import views

urlpatterns = [
    path('', views.main),
]