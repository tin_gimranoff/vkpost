from django.shortcuts import render


def main(request):
    meta = {
        'title': u'Рассылка сообщений пользователям ВК'
    }
    return render(request, 'messaging.html', {
        'meta': meta
    })